#!/usr/bin/env python
from setuptools import find_packages, setup

setup(
    name='pfencoding',
    packages=find_packages(exclude=['tests']),
    install_requires=['smtutils', 'networkx', 'dwave-networkx'],
    test_suite='tests', )
