import pfencoding.searchpf as searchpf
import random
from itertools import product
import networkx as netx
from six.moves import xrange

def expand_(n):
    vals = [(False, True)] * n
    return product(*vals)

def main(args):
    max_nx = 7
    max_na = 7
    for nx in range(4, max_nx+1):
        for _ in xrange(100):
            random_table = dict()
            for assg in expand_(nx-1):
                random_table[assg] = random.choice([True, False])

            def randomfunc(X):
                return X[0] == random_table[tuple(X[1:])]



            model, pf  = searchpf.search_pf_smallest_noarch(nx, max_na, randomfunc)
            if model:
                print (nx, model["ancilla_used"], model)
            else:
                print (nx, None)


if __name__ == '__main__':
    main([])