import networkx
from dwave_networkx.generators.pegasus import pegasus_graph
from dwave_networkx.generators.chimera import chimera_graph
from networkx import OrderedGraph, complete_graph
from networkx.algorithms.isomorphism import GraphMatcher
from smtutils.parsing import SmtResponseParser
from smtutils.process import Solver, get_msat_path
from networkx_nauty.nauty_sparse import SparseGraph
from pfencoding.penaltyfunction import PenaltyFunction
from smtutils.formula import SmtFormula
from collections import defaultdict
from pfencoding.variableelimination import VariableEliminationConstraints
from datetime import datetime

from pfencoding.constraints import RangeConstraint, MinimizeAncillas, ArchitectureConstraint, PfConstraint

from pfencoding.enum_placements import ExplicitPos, generate_all_pos, McKayEnum
from pfencoding.searchpf import search_pf_movable, search_pf_ve_all_pos
from pfencoding.symmetries import ChimeraSymBreak, AncillaSymBreak

SMT_INVOCATIONS = 0
import sys



class AtLeast(object):
    def __init__(self, n, m):
        assert 1 <= n < m
        self.n = n
        self.m = m

    def __call__(self, X):
        assert len(X) == self.m
        return X.count(True) >= self.n

    def __repr__(self):
        return "ATLEAST-{}-IN-{}".format(self.n, self.m)

class Exactly(AtLeast):
    def __call__(self, X):
        assert len(X) == self.m
        return X.count(True) == self.n

    def __repr__(self):
        return "EXACTLY-{}-IN-{}".format(self.n, self.m)


def is_subgraph(small_g, big_g):
    for e in small_g.edges():
        if not big_g.has_edge(e):
            return False
    return True

def greedy_dense_subgraph(g, n):
    ret = networkx.Graph(g)
    while ret.number_of_nodes() > n:
        for node in sorted(ret.nodes(), key=ret.degree):
            ret2 = ret.copy()
            ret2.remove_node(node)
            if networkx.is_connected(ret2):
                ret = ret2
                break
    return ret

class FixFirst(PfConstraint):
    def constraints(self, pf):  # type: (PenaltyFunction) -> Iterable[ValidNode]
        yield pf.pos[0] == 37

def main2():
    graph = pegasus_graph(2, coordinates=True)#.subgraph({
        #(1,1,0,0),
        #(1,1,1,0),
        #(1,1,2,0),
        #(1,1,3,0),
        #(1,0,8,0),
        #(1,0,9,0),
        #(1,0,10,0),
        #(1,0,11,0),
        #(0,1,0,0),
        #(0,1,1,0),
        #(0,1,2,0),
        #(0,1,3,0),
        #(0,0,8,0),
        #(0,0,9,0),
        #(0,0,10,0),
        #(0,0,11,0),
        #})
    print(f"hardware graph has {len(graph)} nodes")
    graph = OrderedGraph(graph)
    extraconstr = [AncillaSymBreak()]
    maxk = 3
    tot_ancilla = 9
    maxsize = 3*maxk + tot_ancilla
    best_a_prev = dict()
    all_subg = defaultdict(set)

    #print(f"{datetime.now().time()}: Generating subgraphs up to {maxsize} nodes...")
    #for subg in McKayEnum(graph).scan(set(), maxsize):
    #    nxsubg = graph.subgraph(subg)
    #    canong = SparseGraph(nxsubg).get_canon()
    #    all_subg[len(subg)].add(canong)

    print(f"{datetime.now().time()}: Greedily pick subgraphs up to {maxsize} nodes...")
    for size in range(1,maxsize+1):
        all_subg[size] = [greedy_dense_subgraph(graph, size)]

    all_subg2 = dict()

    #print(f"{datetime.now().time()}: Filtering with subgraph isomorphism...")
    #i = 0
    #for length, subgs in all_subg.items():
    #    subgs = [g.to_networkx() for g in subgs]
    #    subgs.sort(key= lambda g: g.number_of_edges())
    #    subgs_filtered = []
    #    for subg in reversed(subgs):
    #        if not any(GraphMatcher(prev_g, subg).subgraph_is_isomorphic()
    #                   for prev_g in subgs_filtered):
    #            subgs_filtered.append(subg)
    #    i += len(subgs_filtered)
    #    all_subg2[length] = subgs_filtered
    #print(f"{datetime.now().time()}: done, total of {i} subgraphs")

    #for length, subgs in all_subg.items():
    #    nx_subgs= map(lambda sg: sg.to_networkx(), subgs)
    #    all_subg2[length] = [max(nx_subgs, key=lambda g: g.number_of_edges())]

    #all_subg = all_subg2
    
    for klass in [Exactly]:
        for k in reversed(range(1,maxk+1)): # start from largest, bound ancillas
            for upper in reversed(range(k+1, 3*maxk + 1)):
                nancilla = min(best_a_prev.get((k,upper), tot_ancilla), # has to be better than a previous result 
                               graph.number_of_nodes() - upper) # do not use more than remaining nodes
                if  nancilla < 0:
                    print(f"skipping: {upper} required, {nancilla} ancillas, {graph.number_of_nodes()} available")
                    continue
                constraint = klass(k, upper)
                ex = extraconstr #+ [FixFirst()]
                found = False
                print(f"{datetime.now().time()}: testing {k}-in-{upper} ")
                graphsize = upper+nancilla
                print(f"there are {len(all_subg[graphsize])} subgraphs of size {graphsize}")
                for subg in all_subg[upper + nancilla]:
                    print(f"{datetime.now().time()}: trying graph {subg.nodes()}")
                    sys.stdout.flush()
                    if False:
                        model, pf, pos = search_pf_ve_all_pos(upper, nancilla, subg, constraint, 
                                                      symmetric_vars=[upper], gap=2)#, extraconstr=ex)
                    else:
                        model, pf = search_pf_movable(upper, nancilla, subg,
                                constraint, extraconstr=[ExplicitPos(subg,
                                    [upper, nancilla])], satisfy_gmin=1)
                    if model:
                        found = True
                        size = upper + nancilla
                        print("{!r}: size {}, model {}".format(constraint, size, model))
                        next_na = nancilla - 1 # use less ancilla from now on
                        print(f"using up to {next_na} ancillas from now on below {k}-in-{upper}")
                        for next_upper in range(upper, k, -1):
                            for next_k in range(k, 0, -1):
                                print(next_k, next_upper)
                                best_a_prev[next_k, next_upper] = next_na
                if not found:
                    print(f"{datetime.now().time()}: {constraint}: not found")
                else:
                    print(f"{datetime.now().time()}: {constraint}: found")


if __name__ == '__main__':
    main2()
