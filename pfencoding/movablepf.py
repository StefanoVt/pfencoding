from itertools import combinations

from networkx import OrderedGraph
from smtutils.formula import Symbol

from pfencoding.penaltyfunction import PenaltyFunction, vectproduct, vectcouplings
from pfencoding.constraints import PfConstraint
from six.moves import xrange

class MovablePenaltyFunction(PenaltyFunction):
    "Penalty functions with euf for positions."
    def define_spins(self, nx, na):
        # type: (int, int) -> None
        "Add pos to the variables definitions"
        super(MovablePenaltyFunction, self).define_spins(nx, na)
        self.pos = [Symbol("Int", "pos{}", i) for i in xrange(1, nx+na+1)]

    def define_theta(self, n):
        # type: (int) -> None
        """Define two functions for biases and couplings, and call them to recreate original theta variables
        \( \theta_{ij} \rightarrow \theta(i,j)\) """
        self.offset =  Symbol("Real", "off")
        self.bias_func = Symbol(("Real", "(Int)"), "bias")
        self.biases = [self.bias_func(posi) for posi in self.pos]
        self.coupl_func = Symbol(("Real", "(Int Int)"), "coupl")
        self.couplings = [self.coupl_func( i, j) for i,j in combinations(self.pos, 2)]




class MovableArchConstraints(PfConstraint):
    "Architecture constraints for euf penalty function"
    def __init__(self, graph):
        "Call with nodes and edges of arch graph"
        assert isinstance(graph, OrderedGraph), "needs an orderedgraph"
        self.num_nodes = graph.number_of_nodes()
        self.graph = graph
        #self.noedges = [e for e in combinations(graph.nodes, 2) if not graph.has_edge(e)]

    def constraints(self, pf):
        "Assure that all variables are assigned to graph nodes and that all absent couplings are 0"
        nodes = list(self.graph.nodes())

        for p1, p2 in combinations(pf.pos, 2):
            yield (p1 != p2)

        for i,j in combinations(range(self.num_nodes), 2):
            i += 1
            j += 1
            yield (pf.coupl_func(i,j) == pf.coupl_func(j,i))
            if not self.graph.has_edge(nodes[i-1], nodes[j-1]):
                yield (pf.coupl_func(i, j) == 0)

        for p in pf.pos:
            yield (p >= 0 + 1) & (p < self.num_nodes + 1)

