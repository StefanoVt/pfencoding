from functools import reduce

from .constraints import PfConstraint, isingexpansion, isingtobool
import typing
import networkx as netx
from itertools import combinations
from smtutils.formula import Op, Symbol
import random as rand


def find_elim_order(topology, nx, na):
    # type: (netx.Graph, int, int) -> typing.List[int]
    ret = list(range(nx, nx+na))

    if len(ret) < 2:
        return ret 
    #subt = netx.subgraph(topology, ret)
    subt = topology.copy()
    ret.sort(key=subt.degree)

    #todo better elim order
    ret = anneal_elim_order(subt, ret)
    return ret

def elim_order_cost(top, order):
    order = list(order)
    cost = 0
    while len(order):
        n = order.pop(0)
        cost += 2 ** top.degree(n)
        neighbors = top.neighbors(n)
        neighbors = list(filter(order.__contains__, neighbors))
        if len(neighbors)>0:
            nn = min(neighbors, key=order.index)
            top = netx.contracted_nodes(top, nn, n, self_loops=False)
    return cost

def anneal_elim_order(top, order):
    currcost = elim_order_cost(top, order)
    nnodes = len(order)
    alpha = 0.5
    for i in range(1,10000):
        neworder = list(order)
        n1, n2 = rand.sample(range(nnodes), 2)
        neworder[n1], neworder[n2] = neworder[n2], neworder[n1]
        newcost = elim_order_cost(top, neworder)
        if newcost < currcost or rand.random() < alpha*i:
            currcost = newcost
            order = neworder
    return order



class Bucket(object):
    def __init__(self, val, x_deps, a_deps):
        self.value = val
        self.x_terms = x_deps
        self.a_terms  = a_deps
        self.msg_dependencies = []

    def get_x_deps(self):
        ret = set()
        ret.update(self.x_terms)
        for msg_dep in self.msg_dependencies:
            ret.update(msg_dep.get_x_deps())
        return ret

    def get_a_deps(self):
        ret = set()
        ret.update(self.a_terms)
        for msg_dep in self.msg_dependencies:
            ret.update(msg_dep.get_a_deps())
            ret.remove(self.value)
        return ret

    def depends_on_ancilla(self):
        return len(self.get_a_deps()) > 0

    def get_all_deps(self):
        ret = set()
        ret.update(self.get_x_deps())
        ret.update(self.get_a_deps())
        return ret

    def add_message(self, otherbucket):
        self.msg_dependencies.append(otherbucket)


    def peg_to(self, elim_order):
        return min(self.get_a_deps(), key=elim_order.index)

    def get_msg_value(self, pf, selfval, assgnm):
        all_deps = self.get_all_deps()
        assert all(x in assgnm for x in all_deps)

        ret = pf.biases[self.value] * selfval
        for x_dep in self.x_terms:
            ret += pf.get_coupling(self.value, x_dep) * selfval * assgnm[x_dep]
        for a_dep in self.a_terms:
            ret += pf.get_coupling(self.value, a_dep) * selfval * assgnm[a_dep]
        assgnm2 = dict(assgnm)
        assgnm2[self.value] = selfval
        for msg_dep in self.msg_dependencies:
            ret += msg_dep.get_msg_name(assgnm2, len(pf.zs))
        return ret

    def msg_symbol(self, assgnm, nz):
        return Symbol('Real', self.get_msg_name(assgnm, nz))

    def get_msg_name(self, assgnm, nz):
        all_deps = self.get_all_deps()
        assstr = "".join('_' if z not in all_deps
                         else '+' if assgnm[z] == 1
                         else '-' for z in range(nz))
        return "msg" + str(self.value) +  assstr

    def get_beta_name(self, assgnm):
        all_deps = self.get_all_deps()
        return "beta" + str(dict((x,assgnm[x]) for x in all_deps))

    def define_message(self, pf):
        all_deps = self.get_all_deps()
        for depval in isingexpansion(len(all_deps)):
            assgm = dict(zip(all_deps, depval))
            low_value = self.get_msg_value(pf, -1, assgm)
            msg_name = self.msg_symbol(assgm, len(pf.zs))
            high_value = self.get_msg_value(pf, 1, assgm)

            yield msg_name <= low_value
            yield msg_name <= high_value
            yield Op('or', (msg_name == low_value),  (msg_name == high_value))








class VariableEliminationConstraints(PfConstraint):
    def __init__(self, graph, nx, na, func, g = 2):
        # type: (nx.Graph, int, int, typing.Callable, int) -> None
        #find elimination order
        assert 0 in graph.nodes(), "graph nodes are not vector indices"
        self.elim_order = find_elim_order(graph, nx, na)
        self.edges = list(graph.edges())
        #define buckets
        self.define_buckets(graph)
        self.base_buckets = self.remove_variables()
        self.nx = nx

        #save func
        self.func = func
        self.g = g
        pass

    def define_buckets(self, graph):
        self.buckets = dict()
        elim_order = self.elim_order
        for i, var in enumerate(elim_order):
            x_deps = []
            a_deps = []
            for neigh in graph.neighbors(var):
                if neigh not in elim_order:
                    x_deps.append(neigh)
                elif elim_order.index(neigh) >  i:
                    a_deps.append(neigh)

            bucket = Bucket(var, x_deps, a_deps)
            self.buckets[var] = bucket

    def remove_variables(self):
        ret = []
        buckets = [self.buckets[i] for i in self.elim_order]
        while len(buckets):
            bucket = buckets.pop(0)
            if bucket.depends_on_ancilla():
                next_a = bucket.peg_to(self.elim_order)
                self.buckets[next_a].add_message(bucket)
            else:
                ret.append(bucket)
        return ret

    def base_messages(self, assgm, nz):
        assgm = dict(zip(range(self.nx), assgm))
        for bucket in self.base_buckets:
            assert not bucket.depends_on_ancilla()
            yield bucket.msg_symbol(assgm, nz)

    def constraints(self, pf):
        #constrain messages
        for bucket in self.buckets.values():
            for constr in bucket.define_message(pf):
                yield constr


        #constrain pf
        nz = len(pf.zs)
        # remove as much computation as possible from exponential foreach
        prebuilt_biases = [(x, 
            (str(pf.biases[x] * -1) , None, str(pf.biases[x]* 1))) 
            for x in range(pf.nx)]
        prebuilt_edges = [(i, j, 
            (str(pf.get_coupling(i,j) * -1), None, str(pf.get_coupling(i,j) * 1))) 
                for i,j in self.edges if i < self.nx and j < self.nx]
        for xvals in isingexpansion(pf.nx):
            base_value = Op("+", pf.offset,
                          *(choice[xvals[x]+1] for x, choice in prebuilt_biases),
                          *(choice[(xvals[i]*xvals[j])+1] for i,j,choice in
                              prebuilt_edges),
                          *self.base_messages(xvals, nz)
                          )
            if self.func(list(map(isingtobool, xvals))):
                yield base_value == 0
            else:
                yield base_value >= self.g
