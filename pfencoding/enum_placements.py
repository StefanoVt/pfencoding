from itertools import combinations
from networkx_nauty import nauty_sparse
from networkx_nauty.nauty_sparse import SparseGraph
import networkx as nx
#import pynauty as nty
#import PyBliss as bliss
from collections import defaultdict, Counter


#given current and previous choices, chech if the graph is isomorphic to a previously visited one
from smtutils.formula import Op

from pfencoding.constraints import PfConstraint

class McKayEnum:
    def __init__(self, graph):
        self.large_graph = graph
        self.symms = SparseGraph(graph)
        #self.large_canonical = self.symms.get_canon()
        #self.large_orbits = self.symms.get_orbits()
        self.scan = self.scan1
        self.memo = dict()
        
    def get_canon_orbits(self,colors):
        #memocols = tuple(map(frozenset, colors))
        #if memocols not in self.memo:
        #    self.memo[memocols] = self.symms.get_canon_orbits(colors)
        #return self.memo[memocols]
        return self.symms.get_canon_orbits(colors)
    
    def scan2(self, subg:set, maxsize): #xxx wrong
        yield subg
        C = set()
        canonnl = self.large_canonical.nodelist
        canon_index = {n:i for i,n in enumerate(canonnl)}
        if subg:
                neighs = set()
                for n in subg:
                    neighs.update(self.large_graph.neighbors(n))
                neighs.difference_update(subg)
        else:
                neighs = list(self.large_graph.nodes())
        for upper_obj in neighs:
            y = subg.union({upper_obj})
            y_canon = frozenset(canon_index[n] for n in y)
            
            if len(y) > maxsize or y_canon in C:
                continue
            
            colors = [[n for n in canonnl if n not in y],list(y)]
            if not colors[0]:
                colors = None
            aut_y = self.symms.get_canon(colors)
                
            if all(aut_y.nodelist.index(upper_obj) <= aut_y.nodelist.index(n) for n in y):
                C.add(y_canon)
        for ycan in C:
            y = set(canonnl[n] for n in ycan)
            yield from self.scan2(y, maxsize)
    
    def scan1(self, subg: set, maxsize, aut_x = None):
        #print(subg, len(subg), maxsize)
        yield subg 
        lg = self.large_graph
        if len(subg) < maxsize:
            if subg:
                neighs = set()
                for n in subg:
                    neighs.update(self.large_graph.neighbors(n))
                neighs.difference_update(subg)
                #aut_x = self.symms.get_orbits([[n for n in lg.nodes() if n not in subg], list(subg)])
            else:
                neighs = list(self.large_graph.nodes())
                #aut_x = self.symms.get_orbits()
            #print(aut_x, set(aut_x[n] for n in neighs))
            if aut_x is None:
                aut_x = self.symms.get_orbits()
            
            for upper_obj in set(aut_x[n] for n in neighs) :
                y = subg.union( {upper_obj})
                colors = [[n for n in lg.nodes() if n not in y],list(y)]
                if not colors[0]:
                    colors = None
                aut_y_canon, aut_y = self.get_canon_orbits(colors)
                #aut_y = self.symms.get_orbits(colors)

                m_y = aut_y_canon[min(aut_y_canon.index(n) for n in y)]
                #print(y, upper_obj, m_y, aut_y)
                if  aut_y[upper_obj] == aut_y[m_y]:
                    #print("subg:", y)
                    yield from self.scan1(y, maxsize, aut_y)


def visited(choice, graph, storage, cols):
    g = nauty_sparse.SparseGraph(graph.subgraph(choice))

    uncolored = [n for n in g.nodelist if all(n not in col for col in cols)] #n not in choice and
    colors = list(cols) #+ [list(choice)]
    if len(uncolored) > 0:
        colors.insert(0, uncolored)
    cert = g.get_canon(colors)


    if cert not in storage:
        storage.add(cert)
        return False, storage
    else:
        return True, storage

def uncols(graph, cols):
    return list(n for n in graph.nodelist if all(n not in col for col in cols))

#iterate all ways to pick howmany=[a, b,c,..] nodes out of the nodes in slots from a graph
def pick_from_pos(graph, choices, howmany, cols=None):
    if cols is None:
        cols = []

    if sum(len(col) for col in cols) < sum(howmany):
        orbits = graph.get_orbits([uncols(graph, cols)]+cols)
        for choice in set(orbits[c] for c in choices):
            new_choice = set(choices)
            new_choice.discard(choice)
            if len(cols) == 0 or howmany[len(cols)-1] == len(cols[-1]):
                new_cols =  cols + [[choice]]
            else:
                new_cols = list(cols)
                new_cols[-1] = list(new_cols[-1]) + [choice]

            for next_choice in pick_from_pos(graph, new_choice, howmany,new_cols):
                yield (choice,) + next_choice
    else:
        yield  tuple()

def generate_all_pos(g, symmetries):
    storage = set()
    assert all(sym > 0 for sym in symmetries), f"empty symmetry {symmetries}"
    sparse_g = nauty_sparse.SparseGraph(g)
    visited = set()
    for pos in pick_from_pos(sparse_g, g.nodes(), symmetries):
        cols = [[n for n in g.nodes() if n not in pos]]
        if len(cols[0]) == 0:
            cols.pop(0)
        acc = 0
        for sm in symmetries:
            cols.append(pos[acc:(acc+sm)])
            acc += sm
        canon_pos = sparse_g.get_canon(cols)
        if canon_pos not in visited:
            yield  pos
        else:
            visited.add(canon_pos)


class ExplicitPos(PfConstraint):
    "Explicitly list all non-isomorphic positionings"
    def __init__(self, ograph, symmetries):
        self.symmetries = symmetries
        self.graph = nauty_sparse.SparseGraph(ograph)

    def constraints(self, pf):
        for assrt in self.sub_constraints(pf, 0,  [list(self.graph.nodelist)]):
            yield Op("or", *assrt)

    def sub_constraints(self, pf, i, colors):
        orbit = self.graph.get_orbits(colors)
        candidates = {orbit[c] for c in colors[0]} # uncolored nodes
        assert all(c in colors[0] for c in candidates), (colors, candidates)
        yield [pf.pos[i] == c for c in candidates]

        for candidate in candidates:
            new_unc = list(colors[0])
            new_unc.remove(candidate)
            old_colors = colors[1:]
            if old_colors and (len(old_colors[-1]) <
                    self.symmetries[len(old_colors) - 1]):
                # add candidate to old color
                old_colors[-1] = old_colors[-1] +[candidate]
            else:
                # add new color
                old_colors.append([candidate])
            new_colors = [new_unc] + old_colors
            if i < (pf.nx + pf.na -1):
                assume_candidate = ~(pf.pos[i] == candidate)
                for subpos in self.sub_constraints(pf, i+1, new_colors):
                    yield (assume_candidate, *subpos)
