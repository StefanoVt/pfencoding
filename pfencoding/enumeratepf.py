"""Variant of searchpf for enumerating penalty functions for all possible placements"""
from kiwisolver import Solver

import networkx
from smtutils.formula import SmtFormula
from smtutils.parsing import SmtResponseParser
from smtutils.process import get_msat_path

from pfencoding.constraints import RangeConstraint, ArchitectureConstraint
from pfencoding.enum_placements import generate_all_pos
from pfencoding.penaltyfunction import PenaltyFunction
from pfencoding.variableelimination import VariableEliminationConstraints


def enumerate_pf(nx, na, graph, func, symmetric_vars=None, inner_vars=None, extraconstr=tuple()):
    '''(variant of search_pf_ve that prints all possible placements'''
    # TODO: actually enumerate
    if inner_vars is None:
        inner_vars = set()

    if symmetric_vars is None:
        symmetric_vars = [1] * nx
    symmetric_vars += [na]

    sup_constr = [RangeConstraint()] + list(extraconstr)

    for positioning in generate_all_pos(graph, symmetric_vars):
        if any(nxpos in inner_vars for nxpos in positioning[:nx]): continue
        posgraph = networkx.Graph(graph)
        posgraph.remove_nodes_from(n for n in graph.nodes if n not in positioning)
        posgraph = networkx.relabel_nodes(posgraph, dict(zip(positioning, range(nx + na))), copy=True)
        # networkx.relabel_nodes(posgraph, dict(zip(range(nx+na), positioning)), copy=False)
        sub_constr = [VariableEliminationConstraints(posgraph, nx, na, func, 2), ArchitectureConstraint(posgraph)]
        f = SmtFormula()
        pf = PenaltyFunction(nx, na)

        for ctype in sub_constr + sup_constr:
            for constr in ctype.constraints(pf):
                f.assert_(constr)

        slv = Solver(get_msat_path("optimathsat"))
        f.check_sat()
        f.get_values(*pf.thetas)
        print(positioning, len(str(f)))
        res = slv.run_formula(str(f))
        resp = SmtResponseParser(res)
        model = resp.model
        if model:
            return model, pf, posgraph

    return None, None, None
