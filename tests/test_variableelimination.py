from unittest import TestCase

from pfencoding.enum_placements import ExplicitPos
from pfencoding.movablepf import MovablePenaltyFunction
from pfencoding.variableelimination import *
from pfencoding.penaltyfunction import PenaltyFunction
from pfencoding.searchpf import search_pf_ve_all_pos
from pfencoding.enum_placements import generate_all_pos
import networkx as nx
from dwave_networkx.generators.chimera import chimera_graph

def create(m,n):
    return chimera_graph(m,n), None

class Exactly:
    def __init__(self, n):
        self.n = n

    def __call__(self, x):
        return x.count(True) == self.n

class TestVariableElimination(TestCase):
    def test_bucket(self):
        g, _ = create(2,1)
        pf = PenaltyFunction(8,8)
        perm = [0, 1, 2, 3, 8, 9, 10, 11, 4, 5, 6, 7, 12, 13, 14, 15]
        g = nx.relabel_nodes(g, dict(enumerate(perm)), copy=True)
        ve = VariableEliminationConstraints(g, 8, 8, lambda x: x.count(True) == 1)
        print(ve.elim_order)
        for constr in  map(str, ve.constraints(pf)):
            print(constr)

    def test_big(self):
        g, _ = create(1,2)
        func = Exactly(1)
        print(search_pf_ve_all_pos(8, 16 - 8, g, func, [8]))

    def test_small(self):
        g, _ = create(1,1)
        func = Exactly(2)
        print(search_pf_ve_all_pos(4, 2, g, func, [4]))


    def test_enum(self):
        g, _ = create(2, 1)
        pf = MovablePenaltyFunction(4, 4)
        og = nx.OrderedGraph(g)
        constr = ExplicitPos(og, [4,4])
        for c in map(str, constr.constraints(pf)):
            print(c)

    def test_enum2(self):
        g, _ = create(1, 1)
        allpos = list(generate_all_pos(g, [1]*8))
        print(len(allpos), allpos)
        allpos = list(generate_all_pos(g, [6]))
        print(len(allpos), allpos)
