from unittest import TestCase
from pfencoding.movablepf import MovablePenaltyFunction
from pfencoding.symmetries import SingleTileSymBreak, ChimeraSymBreak
from dwave_networkx.generators.chimera import chimera_graph
from six.moves import map
class TestSymmetries(TestCase):
    def setUp(self):
        self.pf = MovablePenaltyFunction(5,3)

    def test1(self):
        pf = self.pf
        rc = SingleTileSymBreak()
        print(list(map(str, rc.constraints(pf))))

    def test2(self):
        pf = self.pf
        rc = ChimeraSymBreak(1,2)
        g=chimera_graph(1,2)
        print(g.edges())
        for constr in map(str, rc.constraints(pf)):
            print(constr)
