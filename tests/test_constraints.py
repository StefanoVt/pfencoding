from unittest import TestCase
from pfencoding.penaltyfunction import PenaltyFunction
from pfencoding.constraints import RangeConstraint, ExpansionGapConstraint, ArchitectureConstraint, MinimizeAncillas
from smtutils.formula import SmtFormula
from smtutils.process import Solver, get_msat_path
from smtutils.parsing import SmtResponseParser
from dwave_networkx.generators.chimera import chimera_graph

def mapl(f, p):
    return list(map(f,p))

class TestConstraints(TestCase):
    def setUp(self):
        self.pf = PenaltyFunction(3,2)

    def test1(self):
        pf = self.pf
        rc = RangeConstraint()
        print(mapl(str, rc.constraints(pf)))

    def test2(self):
        pf = self.pf

        def function1(t):
            (a, b, c) = t
            return c == (a and b)

        cs = ExpansionGapConstraint(function1, 2)
        print(list(map(str, cs.constraints(pf))))

    def test_arch(self):
        pf = PenaltyFunction(5,3)
        g= chimera_graph(1,1)
        ac = ArchitectureConstraint(g)
        print(list(map(str, ac.constraints(pf))))

    def test_minimize_ancillas(self):
        pf = PenaltyFunction(5, 5)
        mac = MinimizeAncillas(5)
        print(list(map(str, mac.constraints(pf))))


