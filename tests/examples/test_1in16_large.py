from unittest import TestCase

import networkx as nx
from smtutils.formula import SmtFormula
from smtutils.parsing import SmtResponseParser
from smtutils.process import Solver, get_msat_path
from dwave_networkx.generators.chimera import chimera_graph
from pfencoding.constraints import RangeConstraint, ArchitectureConstraint
from pfencoding.penaltyfunction import PenaltyFunction
from pfencoding.utils import print_pf
from pfencoding.variableelimination import VariableEliminationConstraints


class TestExample1in16(TestCase):
    """Example encoding of and2 function"""
    def test_ancilla(self):
        func = (lambda x: x.count(True) == 1)
        g = chimera_graph(1,2)
        perm = [0, 1, 2, 3, 8, 9, 10, 11, 4, 5, 6, 7, 12, 13, 14, 15]
        g = nx.relabel_nodes(g, dict(enumerate(perm)), copy=True)
        assert  g.degree(0) == 4 and g.degree(7) == 4, g.edges()

        f = SmtFormula()
        pf = PenaltyFunction(8, 8)
        constr_types = [RangeConstraint(), VariableEliminationConstraints(g, 8, 8, func)]
        if g is not None:
            archcon = ArchitectureConstraint(g)
            constr_types.append(archcon)
        for ctype in constr_types:
            for constr in ctype.constraints(pf):
                f.assert_(constr)

        slv = Solver(get_msat_path("optimathsat"))
        f.check_sat()
        f.get_values(*pf.thetas)
        res = slv.run_formula(str(f))
        resp = SmtResponseParser(res)
        model = resp.model
        if model:
            print_pf(pf, model, func)
